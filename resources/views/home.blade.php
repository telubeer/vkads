@extends('layouts.app')

@section('content')
<div class="container">
    Вход не выполнен
    <a href="{{ route('login') }}"
       onclick="event.preventDefault();
                                                     document.getElementById('login-form').submit();">
        Авторизоваться
    </a>

    <form id="login-form" action="{{ route('login') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
@endsection
