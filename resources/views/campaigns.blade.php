@extends('layouts.app')
@section('content')
    @isset($error)
        <div class="error">{{ $error }}</div>
    @endisset
    <a href="{{ route('accounts') }}">К списку кабинетов</a>
    <div>Кабинет: {{$account['account_name']}}</div>
    @forelse ($campaigns as $row)
        <div>
            <a href="{{ route('campaign.show', ['id' => $row['id'], 'aid' => $account['account_id']]) }}" >
                {{$row['name']}}
            </a>
        </div>
    @empty
        <p>Рекламных кампаний нет</p>
    @endforelse
@endsection