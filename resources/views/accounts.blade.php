@extends('layouts.app')
@section('content')
    @isset($error)
        <div class="error">{{ $error }}</div>
    @endisset
    <div>Имя: {{$user['name']}}</div>
    <img class="avatar" src="{{$user['avatar']}}" alt="">
    @forelse ($accounts as $row)
        <div>
            <a href="{{ route('campaigns', ['id' => $row['account_id']]) }}" >
                {{$row['account_name']}}
            </a>
        </div>
    @empty
        <p>Рекламных кабинетов нету</p>
    @endforelse
@endsection
