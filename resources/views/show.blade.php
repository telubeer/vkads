@extends('layouts.app')
@section('content')
    @isset($error)
        <div class="error">{{ $error }}</div>
    @endisset
    <a href="{{ route('campaigns', ['id' => $aid]) }}">К списку кампаний</a>
    @forelse ($campaigns as $row)
        <h1>Название компании: {{$row['name']}}</h1>
        <hr>
        <strong>Объявления:</strong>
        @forelse ($row['ads'] as $ad)
            <dl id="dl_{{$ad['id']}}">
                <dt>Название</dt>
                <dd>{{$ad['name']}}</dd>
                <dt>Дневной лимит</dt>
                <dd>{{$ad['day_limit']}}</dd>
                <dt>Лимит объявления</dt>
                <dd>{{$ad['all_limit']}}</dd>
                <dt>Цена за переход</dt>
                <dd>{{$ad['cpc']}}</dd>
                <dt>Статус</dt>
                <dd>{{$ad['status']}}</dd>
                <dt>Дата запуска</dt>
                <dd>{{$ad['start_time']}}</dd>
                <dt>Дата остановки</dt>
                <dd>{{$ad['stop_time']}}</dd>
                <dt>Тематики</dt>
                <dd>{{implode(',', $ad['categories'])}}</dd>
                <dt>Целевая аудитория</dt>
                <dd>{{$ad['count']}}</dd>
                <dt>Платформа:</dt>
                <dd>{{$ad['platform']}}</dd>
                <dt>Демография:</dt>
                <dd>{{$ad['target']}}</dd>
                <dt>Города</dt>
                <dd>{{$ad['cities']}}</dd>
                <dt>Ссылка</dt>
                <dd><a href="{{$ad['link_url']}}" target="_blank">{{$ad['link_url']}}</a></dd>
                <dt>Заметка:</dt>
                <dd>
                    <textarea title="Заметка" id="comment_{{$ad['id']}}" cols="40"
                              rows="2">{{$ad['comment']}}</textarea>
                    <button id="button_{{$ad['id']}}" type="button" onclick="save({{$ad['id']}})">Сохранить</button>
                    <div id="result_{{$ad['id']}}"></div>
                </dd>
                <button id="remove_{{$ad['id']}}" type="button" onclick="remove({{$ad['id']}})">Удалить</button>
            </dl>
        @empty
            <p>Объявлений нет</p>
        @endforelse
    @empty
        <p>Кампаний нет</p>
    @endforelse

    <script>
        var _token = '{{csrf_token()}}';

        @verbatim
        function remove(id) {
            $('#remove_' + id).prop('disabled', true);
            $.ajax({
                url: '/ads/' + id,
                data: {_token},
                type: 'DELETE',
                success: function () {
                    $('#dl_' + id).remove();
                },
                error: function (err) {
                    console.log(err);
                    alert('не удалось удалить');
                }
            });
        }

        function save(id) {
            var button = $('#button_' + id);
            button.prop('disabled', true);
            var textarea = $('#comment_' + id);
            textarea.css('border', '1px solid rgb(169, 169, 169)');
            var value = textarea.val();
            var result = $('#result_' + id);
            result.text('');
            $.post('/comment/' + id, {value, _token}, function (r) {
                button.prop('disabled', false);
                result.text('Сохранено');
                setTimeout(() => {
                    result.text('');
                }, 3000)
            }, 'json').fail(function (err) {
                textarea.css('border', '1px solid red');
                button.prop('disabled', false);
                result.text('Ошибка, ' + err.responseJSON.errors.value[0]);
            })
        }
        @endverbatim
    </script>
@endsection
