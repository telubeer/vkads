<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use VK\OAuth\Scopes\VKOAuthUserScope;

class AuthController extends Controller
{
    const VK_ERROR = 'Ошибка, попробуйте еще раз';

    public function index()
    {
        $user = Auth::user();
        if (!empty($user)) {
            return redirect('/accounts');
        }
        return view('home');
    }

    public function login()
    {
        return Socialite::driver('vkontakte')
            ->setScopes([VKOAuthUserScope::ADS])
            ->redirect();
    }

    public function code()
    {
        $data = Socialite::driver('vkontakte')->user();
        event(new Registered($user = $this->findOrCreate($data)));
        Auth::guard('web')->login($user, true);
        return redirect('/accounts');
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  $data
     * @return \App\User
     */
    protected function findOrCreate($data)
    {
        $user = User::find($data['id']);
        if (!empty($user)) {
            $user->token = $data->token;
            $user->save();
            return $user;
        }
        return User::create([
            'id' => $data->id,
            'name' => $data->name,
            'avatar' => $data->avatar,
            'token' => $data->token,
        ]);
    }
}