<?php

namespace App\Http\Controllers;

use App\Vkads\AdsService;
use App\Vkads\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiWeightedFloodException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class AccountsController extends Controller
{
    const VK_ERROR = 'Ошибка, попробуйте еще раз';

    /**
     * @param AdsService $client
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function accounts(AdsService $client)
    {
        $error = '';
        $accounts = [];
        try {
            $accounts = $client->getAccounts();
        } catch (VKApiException $e) {
            $error = static::VK_ERROR;
        } catch (VKClientException $e) {
            $error = static::VK_ERROR;
        }
        return view('accounts', [
            'accounts' => $accounts,
            'user' => Auth::user(),
            'error' => $error
        ]);
    }

    /**
     * @param AdsService $client
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function campaigns(AdsService $client, $id) {
        $data = [];
        $error = '';
        $account = [];
        try {
            $accounts = $client->getAccounts();
            $account = $accounts[$id];
            $data = $client->getCampaigns($id);
        } catch (VKApiWeightedFloodException $e) {
            $error = static::VK_ERROR;
        } catch (VKApiException $e) {
            $error = static::VK_ERROR;
        } catch (VKClientException $e) {
            $error = static::VK_ERROR;
        }
        return view('campaigns',
            ['campaigns' => $data, 'account' => $account, 'error' => $error]
        );
    }

    public function show(AdsService $client, $id, $aid) {
        $data = [];
        $error = '';
        try {
            $data = $client->getInfo($id, $aid);
        } catch (VKApiWeightedFloodException $e) {
            $error = static::VK_ERROR;
        } catch (VKApiException $e) {
            $error = static::VK_ERROR;
        } catch (VKClientException $e) {
            $error = static::VK_ERROR;
        }
        return view('show', [
            'aid' => $aid,
            'campaigns' => $data,
            'error' => $error
        ]);
    }

    /**
     * @param AdsService $client
     * @param $id
     * @return string
     * @throws VKApiException
     * @throws VKApiWeightedFloodException
     * @throws VKClientException
     */
    public function delete(AdsService $client, $id) {
        $client->delete($id);
        return 'deleted ' . $id;
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function save($id, Request $request) {
        $comment = $this->findOrCreate($id);
        $validatedData = $request->validate([
            'value' => 'required|max:100',
        ]);
        $comment['value'] = $validatedData['value'];
        $comment->save();
        return response('', 204, []);
    }

    protected function findOrCreate($id) {
        $comment = Comment::find($id);
        if (!empty($comment)) {
            return $comment;
        }
        return Comment::create(['id' => $id]);
    }
}