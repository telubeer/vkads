<?php

namespace App\Providers;

use App\Vkads\AdsService;
use Illuminate\Support\ServiceProvider;
use VK\Client\VKApiClient;
use VK\OAuth\VKOAuth;

class VkSdkServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(VKApiClient::class, function ($app) {
            return new VKApiClient();
        });
        $this->app->singleton(VKOAuth::class, function ($app) {
            return new VKOAuth();
        });
        $this->app->singleton(AdsService::class, function ($app) {
            return new AdsService($app[VKApiClient::class]);
        });
    }
}
