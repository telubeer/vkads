<?php

namespace App\Vkads;

use VK\Client\VKApiClient;

class AdsService {
    const CACHE_TIME_IN_MINUTES = 5;
    /**
     * @var VKApiClient
     */
    protected $client;
    /**
     * @var string
     */
    protected $token;

    /**
     * AdsService constructor.
     * @param VKApiClient $client
     */
    public function __construct(VKApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param $token
     */
    public function setToken($token) {
        $this->token = $token;
    }

    /**
     * @param $cid
     * @param $aid
     * @return array
     * @throws \VK\Exceptions\Api\VKApiWeightedFloodException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function getInfo($cid, $aid) {
        $campaigns = $this->getCampaigns($aid);
        $campaigns = array_filter($campaigns, function($row) use ($cid) {
            return $row['id'] == $cid;
        });
        $ads = $this->getAdsData($aid, $cid);
        return array_map(function($row) use ($ads) {
            $row['ads'] = $ads[$row['id']] ?? [];
            return $row;
        }, $campaigns);
    }

    /**
     * @param array $ids
     * @return array|\Illuminate\Cache\CacheManager|mixed
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function getCities(array $ids) {
        $cacheKey = __METHOD__ . $this->token;
        $result = cache($cacheKey);
        if (!empty($result)) {
            return $result;
        }
        $data = $this->client->database()
            ->getCitiesById($this->token, ['city_ids' => $ids]);
        $result = [];
        foreach ($data as $row) {
            $result[$row['id']] = $row['title'];
        }
        cache([$cacheKey => $result], static::CACHE_TIME_IN_MINUTES);
        return $result;
    }

    /**
     * @return array|\Illuminate\Cache\CacheManager|mixed
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     * @throws \Exception
     */
    public function getAccounts() {
        $cacheKey = __METHOD__ . $this->token;
        $result = cache($cacheKey);
        if (!empty($result)) {
            return $result;
        }
        $data = $this->client->ads()
            ->getAccounts($this->token);
        $result = [];
        foreach ($data as $row) {
            $result[$row['account_id']] = $row;
        }
        cache([$cacheKey => $result], static::CACHE_TIME_IN_MINUTES);
        return $result;
    }

    /**
     * @param $aid
     * @return mixed
     * @throws \VK\Exceptions\Api\VKApiWeightedFloodException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     * @throws \Exception
     */
    public function getCampaigns($aid) {
        $cacheKey = __METHOD__ . $aid;
        $result = cache($cacheKey);
        if (!empty($result)) {
            return $result;
        }
        $result = $this->client->ads()
            ->getCampaigns($this->token, ['account_id' => $aid]);
        cache([$cacheKey => $result], static::CACHE_TIME_IN_MINUTES);
        return $result;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \VK\Exceptions\Api\VKApiWeightedFloodException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     * @throws \Exception
     */
    public function delete($id) {
        $this->client->ads()->
        deleteAds($this->token, ['ids' => json_encode([$id])]);
        cache()->flush();
    }

    /**
     * @param $aid
     * @param $cid
     * @return mixed
     * @throws \VK\Exceptions\Api\VKApiWeightedFloodException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    protected function getAdsData($aid, $cid)
    {
        $categories = $this->getCategories();
        $targeting = $this->getAdsTargeting($aid, $cid);
        $cityids = $this->parseCities($targeting);
        $cities = $this->getCities($cityids);
        $layout = $this->getAdsLayout($aid, $cid);
        $result = $this->getAds($aid, $cid);
        $ids = array_map(function ($row) {
            return $row['id'];
        }, $result);
        $comments = $this->getComments($ids);
        $ads = [];
        foreach ($result as $row) {
            $row['comment'] = $comments[$row['id']] ?? '';
            $row['ad_format'] = $layout[$row['id']]['ad_format'] ?? null;
            $row['link_url'] = $layout[$row['id']]['link_url'] ?? null;
            $row['targeting'] = $targeting[$row['id']] ?? null;
            $row['categories'][] = $categories[$row['category1_id']] ?? null;
            $row['categories'][] = $categories[$row['category2_id']] ?? null;
            $row['categories'] = array_filter($row['categories']);
            Decorator::decorate($row, $cities);
            $ads[$row['campaign_id']][] = $row;
        }
        return $ads;
    }

    protected function getComments($ids) {
        $comments = Comment::whereIn('id', $ids)->get();
        $result = [];
        foreach ($comments as $row) {
            $result[$row['id']] = $row['value'];
        }
        return $result;
    }

    /**
     * @param $aid
     * @param $cid
     * @return mixed
     * @throws \VK\Exceptions\Api\VKApiWeightedFloodException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    protected function getAdsTargeting($aid, $cid) {
        $cacheKey = __METHOD__ . $aid . $cid;
        $result = cache($cacheKey);
        if (!empty($result)) {
            return $result;
        }
        $result = $this->client->ads()->getAdsTargeting(
            $this->token, [
                'account_id' => $aid,
                'campaign_ids' => json_encode([$cid])
            ]
        );
        foreach ($result as $row) {
            $result[$row['id']] = $row;
        }
        cache([$cacheKey => $result], static::CACHE_TIME_IN_MINUTES);
        return $result;
    }

    /**
     * @param $aid
     * @param $cid
     * @return \Illuminate\Cache\CacheManager|mixed
     * @throws \VK\Exceptions\Api\VKApiWeightedFloodException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    protected function getAds($aid, $cid) {
        $cacheKey = __METHOD__ . $aid . $cid;
        $result = cache($cacheKey);
        if (!empty($result)) {
            return $result;
        }
        $result = $this->client->ads()->getAds(
            $this->token,
            [
                'account_id' => $aid,
                'campaign_ids' => json_encode([$cid])
            ]);
        cache([$cacheKey => $result], static::CACHE_TIME_IN_MINUTES);
        return $result;
    }

    /**
     * @param $aid
     * @param $cid
     * @return \Illuminate\Cache\CacheManager|mixed
     * @throws \VK\Exceptions\Api\VKApiWeightedFloodException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    protected function getAdsLayout($aid, $cid) {
        $cacheKey = __METHOD__ . $aid . $cid;
        $result = cache($cacheKey);
        if (!empty($result)) {
            return $result;
        }
        $result = $this->client->ads()
            ->getAdsLayout(
                $this->token,
                [
                    'account_id' => $aid,
                    'campaign_ids' => json_encode([$cid])
                ]
            );
        foreach ($result as $row) {
            $result[$row['id']] = $row;
        }
        cache([$cacheKey => $result], static::CACHE_TIME_IN_MINUTES);
        return $result;
    }

    /**
     * @return mixed
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     * @throws \Exception
     */
    protected function getCategories()
    {
        $result = cache(__METHOD__);
        if (!empty($result)) {
            return $result;
        }
        ['v1' => $v1, 'v2' => $v2] = $this->client->ads()
            ->getCategories($this->token, ['lang' => 'ru']);
        $result = [];
        $this->parseCategories(array_merge($v1, $v2), $result);
        cache()->forever(__METHOD__, $result);
        return $result;
    }

    /**
     * @param $rows
     * @param $result
     */
    protected function  parseCategories($rows, &$result) {
        foreach ($rows as $row) {
            $result[$row['id']] = $row['name'];
            if (empty($row['subcategories'])) {
                continue;
            }
            $this->parseCategories($row['subcategories'], $result);
        }
    }

    /**
     * @param $targeting
     * @return array
     */
    protected function parseCities($targeting): array
    {
        $cityids = [];
        foreach ($targeting as $row) {
            if (empty($row['cities'])) {
                continue;
            }
            $exploded = explode(',', $row['cities']);
            $cityids = array_merge($cityids, $exploded);
        }
        return array_filter(array_unique($cityids));
    }


}