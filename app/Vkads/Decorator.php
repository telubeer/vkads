<?php

namespace App\Vkads;
class Decorator
{
    public static function decorate(&$row, array $cities)
    {
        $row['platform'] = static::platform($row);
        $row['start_time'] = static::time($row['start_time']);
        $row['stop_time'] = static::time($row['stop_time']);
        $row['count'] = static::count($row['targeting']['count']);
        $row['day_limit'] = static::currency($row['day_limit']);
        $row['all_limit'] = static::currency($row['all_limit']);
        $row['target'] = static::targeting($row['targeting']);
        $row['cities'] = static::cities($row['targeting']['cities'] ?? [], $cities);
        if (!empty($row['cpc'])) {
            $row['cpc'] = static::currency($row['cpc'] / 100);
        }
    }

    public static function cities($value, $cities) {
        if (empty($value)) {
            return 'не задано';
        }
        $mapped = array_filter(array_map(function($id) use ($cities) {
            return $cities[$id] ?? null;
        }, explode(',', $value)));
        return implode(', ', $mapped);
    }

    public static function sex($value)
    {
        switch ($value) {
            case 1:
                return 'Женщины';
            case 2:
                return 'Мужчины';
            default:
                return 'не определились';
        }
    }

    public static function targeting($row)
    {
        $out = '';
        if (!empty($row['sex'])) {
            $out .= static::sex($row['sex']);
        }
        if (!empty($row['age_from'])) {
            $out .= ' от ' . $row['age_from'];
        }
        if (!empty($row['age_to'])) {
            $out .= ' до ' . $row['age_to'];
        }
        return empty($out) ? 'не задана' : $out;
    }

    public static function currency($value)
    {
        return number_format($value, 2, ',', ' ') . ' руб.';
    }

    public static function count($value)
    {
        if (empty($value)) {
            return 0;
        }
        return number_format($value, 0, ',', ' ') . ' человек';
    }

    public static function time($value)
    {
        if (empty($value)) {
            return 'не задана';
        }
        return date('d.m.Y H:i', $value);
    }

    /**
     * @param $row
     * @return null|string
     */
    public static function platform($row)
    {
        switch ($row['ad_format']) {
            case 1:
                return $row['ad_platform']
                    ? 'только ВКонтакте'
                    : 'ВКонтакте и сайты-партнёры';
            case 9:
                switch ($row['ad_platform']) {
                    case 'all':
                        return 'все площадки';
                    case 'desktop':
                        return 'полная версия сайта';
                    case 'mobile':
                        return 'мобильный сайт и приложения';
                    default:
                        return null;
                }
            default:
                return null;
        }
    }
}