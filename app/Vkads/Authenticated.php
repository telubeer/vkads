<?php

namespace App\Vkads;
use Illuminate\Auth\Events\Authenticated as Auth;

class Authenticated
{
    protected $service;

    /**
     * Create the event listener.
     *
     * @param AdsService $service
     */
    public function __construct(AdsService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Auth $event
     * @return void
     */
    public function handle(Auth $event)
    {
        $this->service->setToken($event->user['token']);
    }
}
