<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'AuthController@index')->name('home');
Route::post('/login', 'AuthController@login')
    ->name('login');
Route::post('/logout', 'AuthController@logout')
    ->name('logout');
Route::get('/code', 'AuthController@code');

Route::get('/accounts', 'AccountsController@accounts')
    ->name('accounts')
    ->middleware('auth');

Route::get('/accounts/{id}', 'AccountsController@campaigns')
    ->name('campaigns')
    ->middleware('auth');

Route::get('/campaigns/{id}/{aid}', 'AccountsController@show')
    ->name('campaign.show')
    ->middleware('auth');

Route::delete('/ads/{id}', 'AccountsController@delete')
    ->middleware('auth');

Route::post('/comment/{id}', 'AccountsController@save')
    ->middleware('auth');
